import requests, sys
from colorama import Fore, Back, Style, init
from time import sleep
from datetime import datetime
from math import modf

status_descriptions = {
    200: "OK",
    400: "Bad Request",
    401: "Unauthorized",
    403: "Forbidden",
    404: "Not Found",
    405: "Method Not Allowed",
    408: "Request Timeout",
    414: "URI Too Long",
    429: "Too Many Requests",
    500: "Internal Server Error",
    501: "Not Implemented",
    502: "Bad Gateway",
    503: "Service Unavailable",
    504: "Gateway Timeout",
    520: "Web server returned unknown error to Cloudflare",
    521: "Web server refuses connections from Cloudflare",
    522: "Timeout when Cloudflare contacted the origin web server",
    523: "Cloudflare cannot contact origin web server",
    524: "Response timeout occured when Cloudflare connected to origin web server",
    525: "SSL handshake failed between Cloudflare and the origin web server",
    526: "Cloudflare could not validate SSL certificate",
    527: "Railgun Listener to origin error",
    530: "Cloudflare error"
}

refresh_count = 0
errorwait_time = 300 # Time to wait (in seconds) when a server error is detected

init()
start_time = datetime.now()
id64 = input("Enter your ID64: ")

def request_refresh(p_id64):
    global refresh_count
    token = "" # Put your token from https://backpack.tf/connections
    url = f"https://backpack.tf/api/inventory/{p_id64}/refresh?token={token}"
    
    if token == "":
       print(Fore.YELLOW + "You did not set your token in the script! Get it at https://backpack.tf/connections")
       sys.exit()

    # User agent (you can set this to your browser's user agent)
    h = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:108.0) Gecko/20100101 Firefox/108.0'}
    # Time to wait (in seconds) for a response
    t = 15

    try:
        refresh_count += 1
        print(Style.RESET_ALL + Style.BRIGHT)

        print(Fore.CYAN + f"Sending request to API server, {refresh_count} attempts so far.")
        r = requests.post(url, headers=h, timeout=t)
        pt = r.text

        if r.status_code == 200:
            print(Fore.GREEN + f"Got response, status code {r.status_code} ({status_descriptions[r.status_code]})", end="\n----------------\n")
            print(f"{Fore.WHITE + Style.BRIGHT}API response:{Style.NORMAL} {pt}")
        else: 
            if r.status_code in status_descriptions.keys():
                print(Fore.YELLOW + f"Got response, status code {r.status_code} ({status_descriptions[r.status_code]})", end="\n----------------\n")       
            else:
                print(Fore.YELLOW + f"Got response, status code {r.status_code}", end="\n----------------\n")
            
            print(f"{Fore.WHITE + Style.BRIGHT}API response:{Style.NORMAL} {pt}")
            
            if r.status_code >= 400 and r.status_code <= 499:
                print("Client error. Quitting...")
                sys.exit()
            elif r.status_code >= 500 and r.status_code <= 599:
                print(f"Server error, waiting {int(errorwait_time//60)} minutes before retrying.")
                sleep(errorwait_time)
            else:
                print("Unexpected HTTP status code, report this issue if it persists. Quitting...")
                sys.exit()
                
            return request_refresh(p_id64)
            
        return r
    except requests.exceptions.ReadTimeout:
        print(Fore.YELLOW + f"Timed out after {t} seconds, retrying.")
        return request_refresh(p_id64)

def start_refresh(p_id64, p_loopuntilupdated = True):
    result = request_refresh(p_id64)
    
    if result.text is not None:
        json = result.json()

        server_current_time = datetime.fromtimestamp(json['current_time'])
        last_attempt_time = datetime.fromtimestamp(json['last_update'])
        next_attempt_time = datetime.fromtimestamp(json['next_update'])
        last_snapshot_time = datetime.fromtimestamp(json['timestamp'])
        secs_since_lastsnapshot = (server_current_time - last_snapshot_time).total_seconds()

        if p_loopuntilupdated and secs_since_lastsnapshot > 180:
            print(f"{Style.BRIGHT}API server's current time:{Style.NORMAL} {server_current_time.strftime('%d %B, %Y %H:%M:%S')}", end="\n----------------\n")
            print(f"{Style.BRIGHT}Last update attempt time:{Style.NORMAL} {last_attempt_time.strftime('%d %B, %Y %H:%M:%S')}")
            print(f"{Style.BRIGHT}Next possible update:{Style.NORMAL} {next_attempt_time.strftime('%d %B, %Y %H:%M:%S')}")
            print(f"{Style.BRIGHT}Latest snapshot time:{Style.NORMAL} {last_snapshot_time.strftime('%d %B, %Y %H:%M:%S')}", end="\n----------------\n")
            
            time_towait = (next_attempt_time - server_current_time).total_seconds()
            print(f"{Style.BRIGHT}Seconds elapsed since last snapshot:{Style.NORMAL} {secs_since_lastsnapshot} (~{int(secs_since_lastsnapshot//60)} minutes | {int(secs_since_lastsnapshot//86400)} days ago)")
            
            if next_attempt_time > server_current_time:
                print(f"Waiting {int(time_towait//60)} minutes and {int(time_towait % 60)} seconds before next refresh...")
                sleep(time_towait)
            else:
                print(Fore.YELLOW + "Next update time lower than API's current server time, refreshing immediately.")

            start_refresh(p_id64, p_loopuntilupdated)
        else:
            print(f"Finished refreshing {p_id64}. {Fore.GREEN + 'The backpack was properly updated at most 3 minutes ago.' if secs_since_lastsnapshot < 180 else Fore.RED + 'The backpack was not properly updated.'}")
    else:
        print(Fore.YELLOW + f"Got empty response, retrying.")
        start_refresh(p_id64, p_loopuntilupdated)

start_refresh(id64, True)
